Ce document fait suite au document déjà publié sur la forge : [RAG NSI](https://forge.apps.education.fr/informatique-lycee/rag-nsi)

Toute la partie "RAG" est quasiment identique à ce qui a déjà été publié, n'hésitez pas à consulter l'article cité ci-dessus pour plus d'informations.

Vous pouvez tester ce chatbot NSI [ici](https://pixees.fr/informatiquelycee/chatbot/)

# Améliorations par rapport à la version précédente

## plus rapide

La version précédente (voir [RAG NSI](https://forge.apps.education.fr/informatique-lycee/rag-nsi)) était relativement lente, sans doute à cause de l'utilisation de streamlit (cette bibliothèque est très pratique pour faire du prototypage, mais montre vite ses limites en production). Cette nouvelle version utilise un modèle "client-serveur" classique avec l'utilisation de la bibliothèque Python FastAPI côté serveur et du classique trio HTML + CSS + JavaScript côté client.

C'est sans doute le choix de ne plus utiliser streamlit qui explique ce gain en termes de rapidité

## éditeur de code

Un éditeur de code a été ajouté afin de faciliter la saisie de code (Python, SQL...)

# comment utiliser ce chatbot en local ?

Après avoir cloné ce dépôt, vous devez : 

- créez un fichier ``.env`` dans le répertoire ``serveur`` afin de pouvoir renseigner la clé Groq (voir l'article [RAG NSI](https://forge.apps.education.fr/informatique-lycee/rag-nsi) pour plus de précision)

- utilisez le fichier ``requirements.txt`` présent dans le répertoire ``serveur`` afin d'installer les bibliothèques Python nécessaires

- ouvrez une console dans le répertoire ``serveur`` et tapez ``fastapi dev``

- ouvrez une autre console dans le réprtoire ``client`` et tapez ``python3 -m http.server 7800``

- dans la barre de votre navigateur web, taper ``localhost:7800`` 

# comment créer son propre chatbot ?

- dans le répertoire ``serveur``, effacer le contenu du répertoire ``storage``

- toujours dans le répertoire ``serveur``, créez un répertoire ``documents`` et placez vos documents dans ce répertoire

- lancez le serveur comme expliqué ci-dessus et patientez quelques minutes... (cela peut être long)

# mettre en ligne son serveur

Si vous désirez mettre en ligne votre serveur, je vous conseille d'utiliser les "spaces" proposés par [Hugging Face](https://huggingface.co/)






