const input_text = document.querySelector('#input_text');
const send_text = document.querySelector('#send_text');
const valid_code = document.querySelector('#valid_code');
const conversation = document.querySelector('#conversation');
const but_code = document.querySelector('#but_code');
const div_code = document.querySelector('#div_code');

const spinner = document.createElement("div");
spinner.classList.add("inline-block", "ml-2", "h-7", "w-7", "animate-spin", "rounded-full", "border-4", "border-solid", "border-current", "border-e-transparent", "align-[-0.125em]", "text-surface", "motion-reduce:animate-[spin_1.5s_linear_infinite]", "dark:text-white")
spinner.setAttribute('role', "status")
span_spinner = document.createElement("span");
span_spinner.classList.add("!absolute", "!-m-px", "!h-px", "!w-px", "!overflow-hidden", "!whitespace-nowrap", "!border-0", "!p-0", "![clip:rect(0,0,0,0)]")

var myCodeMirror = CodeMirror.fromTextArea(code, {
    lineNumbers: true,
    mode: "python",
    theme: "cobalt"
  });

const chat_memory = [];
let isthinking = false;


async function sendQuestion(choice){
    let question="";
    if (choice == 2){
        question = "```\n"+myCodeMirror.getValue()+"\n```";
        
    }
    else{
        question = input_text.value;
        input_text.value ="";
    }
    
    const p_query = document.createElement("p");
    p_query.innerHTML = marked.parse(question);
    const div_query = document.createElement("div");
    div_query.classList.add("p-4", "text-justify", "rounded-2xl", "bg-slate-100", "ml-44");
    div_query.appendChild(p_query);
    conversation.appendChild(div_query);

    const p_resp = document.createElement("p");
    p_resp.innerHTML = "Patientez SVP, je réfléchis...";
    const img = document.createElement("img");
    img.setAttribute("src","./images/agent.png");
    img.setAttribute("alt","assistant icone");
    img.classList.add("w-8", "h-8", "mr-3");
    const div_resp = document.createElement("div");
    div_resp.classList.add("p-4", "text-justify", "flex");
    div_resp.appendChild(img);
    div_resp.appendChild(p_resp);
    div_resp.appendChild(spinner);
    conversation.appendChild(div_resp);
    div_resp.scrollIntoView();
    isthinking = true;
    try{
        const callChatbot = await fetch("http://localhost:8000/request",{
            method : "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body : JSON.stringify({chat_message : chat_memory, query : question}),
        });
        const response = await callChatbot.json();

        div_resp.removeChild(spinner);
        p_resp.innerHTML = marked.parse(response.response);
        div_resp.scrollIntoView();
 
        chat_memory.push({role: "user", content: question});
        chat_memory.push({role: "assistant", content: response.response});
        console.log(chat_memory)
        isthinking = false;
    } catch (error) {
        div_resp.removeChild(spinner);
        div_resp.classList.add("bg-red-200", "rounded-xl", "mt-5");
        p_resp.innerHTML = "Désolé, un problème interne ne me permet pas de répondre à votre demande, je vous prie de bien vouloir réessayer plus tard (n'hésitez pas à recharger la page d'ici quelques minutes)";
    }
}

let is_code = false;
send_text.addEventListener("click", ()=>{
    if (!isthinking) {
        sendQuestion(1)
    }
});
valid_code.addEventListener("click", ()=>{
    if (!isthinking) {
        sendQuestion(2)
    }
});
but_code.addEventListener("click", ()=>{
    if (is_code){
        div_code.classList.remove("left-0");
        div_code.classList.add("-left-1/4", "transition-all", "duration-500");
    }
    else {
        div_code.classList.remove("-left-1/4");
        div_code.classList.add("left-0", "transition-all", "duration-500");
    }
    is_code = !is_code;
});
input_text.addEventListener('keypress', (e) => {
    if (e.key === 'Enter' && !e.shiftKey && !isthinking) {
        input_text.blur();
        sendQuestion(1);
        return false;
    }
});